import Typography from "typography"

const typography = new Typography({
  baseFontSize: "16px",
  baseLineHeight: 1.666,
  scaleRatio: 3,
  headerFontFamily: [
    "Montserrat",
    "Avenir Next",
    "Helvetica Neue",
    "Segoe UI",
    "Helvetica",
    "Arial",
    "sans-serif",
  ],
  bodyFontFamily: [
    "Maven Pro",
    "Helvetica Neue",
    "Segoe UI",
    "Helvetica",
    "sans-serif"
  ],
  bodyWeight: 200,
  bodyColor: '#737373',
  headerColor: '#353535',
  includeNormalize: true,
  googleFonts: [{
    name: 'Montserrat',
    styles: [
      '500', '700'
    ]
  }, {
    name: 'Maven Pro',
    styles: [
      '500', '200'
    ]
  }],
  overrideStyles: ({
    adjustFontSizeTo,
    rhythm
  }, options, styles) => ({
    hr: {
      borderColor: "#f2f2f2",
      borderStyle: "solid",
      borderWidth: "1px",
      background: "none",
    },
    h1: {
      textAlign: "center"
    },
    h2: {
      marginBottom: ".25em"
    },
    img: {
      margin: 0
    }
  })
})

export default typography
