# Audec.io
Product website for AuDEc at audec.io built with Gatsby.

_Forked from https://github.com/gatsbyjs/gatsby-starter-default_

## Development
1. Install dependencies
    ```bash
    yarn install
    ```

1. Start local server
    ```bash
    yarn dev:start
    ```

## Deployment
1. Install dependencies
    ```bash
    yarn install
    ```

1. Build static site
    ```bash
    yarn build
    ```