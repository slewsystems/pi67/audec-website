module.exports = {
  siteMetadata: {
    title: `AuDEc AES67 Media Platform`,
  },
  plugins: [{
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`
      }
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-typescript',
    'gatsby-plugin-react-helmet'
  ],
}
